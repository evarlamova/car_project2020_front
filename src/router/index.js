import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/rents-page/',
      name: 'rent-cars',
      component: () => import('@/views/RentedCars'),
    },
    {
      path: '/',
      name: 'main-page',
      component: () => import('@/views/MainPage'),
    },
    {
      path: '*',
      component: () => import('@/components/404'),
    }
  ]
});