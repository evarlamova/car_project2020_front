export const START_FETCH = 'START_FETCH';
export const END_FETCH = 'END_FETCH';
export const SAVE_USER = 'SAVE_USER';
export const CLEAR_USER = 'CLEAR_USER';
export const SHOW_LOGIN_MODAL = 'SHOW_LOGIN_MODAL';
export const SAVE_RENTS = 'SAVE_RENTS';
export const SEARCH_CARS = 'SEARCH_CARS';