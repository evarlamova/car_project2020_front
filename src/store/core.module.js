import {
  GET_DATA,
  REGISTER,
  GET_USER,
  LOGIN,
  LOGOUT,
  RENT_CAR,
  GET_RENT_CARS,
  UNRENT_CAR,
} from "./actions.type";
import {
  START_FETCH,
  END_FETCH,
  SAVE_USER,
  SHOW_LOGIN_MODAL,
  CLEAR_USER,
  SEARCH_CARS,
  SAVE_RENTS,
} from "./mutations.type";
import { HTTP } from "@/common/api.service";

const state = {
  test: '',
  isLoading: false,
  user: null,
  cars: [],
  isAuth: false,
  isShowLoginModal: false,
  rentCars: [],
  searchCars: null,
};

const getters = {
  test(state) {
    return state.test;
  },
  isShowLogin(state) {
    return state.isShowLoginModal;
  },
  user(state) {
    return state.user;
  },
  authStatus(state) {
    return state.isAuth;
  },
  cars(state) {
    return state.cars;
  },
  rentCars(state) {
    return state.rentCars;
  },
  rentCarsCount(state) {
    return state.rentCars.length;
  },
  rentCarsIds(state) {
    return state.rentCars.map((item) => item.car.id);
  },
  searchCars(state) {
    return state.searchCars;
  }
};


const actions = {

  [UNRENT_CAR]({ dispatch }, data) {
    return HTTP.delete('/rents/', { data })
      .then(() => dispatch(GET_RENT_CARS))
      .catch(err => alert(err));
  },

  [GET_RENT_CARS]({ commit }) {
    const token = localStorage.getItem('token');
    if (!token) return;
    return HTTP.get('/rents/', {
      headers: {
        'Authorization': `Token ${token}`,
      }
    })
      .then(({ data }) => commit(SAVE_RENTS, data || []))
      .catch(err => alert(err));
  },

  [RENT_CAR]({ dispatch }, data) {
    return HTTP.post('/rent/', data)
      .then(() => {
        dispatch(GET_RENT_CARS);
      })
      .catch(err => alert(err));
  },

  [GET_DATA]({ commit }) {
    return HTTP.get('/cars/')
      .then(({ data }) => commit(END_FETCH, data))
      .catch(err => alert(err));
  },

  [REGISTER]({ commit }, data) {
    return HTTP.post('/auth/users/', data, { useCredentails: true })
      .then((response) => {
        if (response.status === 201) commit(SHOW_LOGIN_MODAL, true);
      })
      .catch(err => {
        if (err.response) {
          alert(JSON.stringify(err.response.data))
        }
      });
  },

  [GET_USER]({ commit }) {
    const token = localStorage.getItem('token');
    if (!token) return;
    return HTTP.get('auth/users/me/', {
      headers: {
        'Authorization': `Token ${token}`,
      }
    })
      .then(({ data }) => {
        commit(SAVE_USER, data);
      })
  },

  [LOGIN]({ dispatch }, data) {
    return HTTP.post('auth/token/login/', data)
      .then(({ data }) => {
        localStorage.setItem('token', data.auth_token);
        dispatch(GET_USER);
        dispatch(GET_RENT_CARS);
      })
      .catch(err => {
        if (err.response) {
          alert(JSON.stringify(err.response.data))
        }
      });
  },

  [LOGOUT]({ commit }) {
    const token = localStorage.getItem('token');
    return HTTP.post('auth/token/logout/', {}, {
      headers: {
        'Authorization': `Token ${token}`,
      }
    })
      .then(() => {
        localStorage.removeItem('token');
        commit(CLEAR_USER);
      })
      .catch(err => {
        if (err.response) {
          alert(JSON.stringify(err.response.data))
        }
      });
  }
};

const mutations = {

  [SAVE_RENTS](state, data) {
    state.rentCars = data;
  },

  [START_FETCH](state) {
    state.isLoading = true;
  },

  [END_FETCH](state, data) {
    const newData = data.map(item => {
      const newItem = { ...item };
      newItem.image[0].file = newItem.image[0].file.split('8000')[1]
      return newItem;
    })
    state.cars = newData;
    state.isLoading = false;
  },

  [SAVE_USER](state, user) {
    state.user = user;
    state.isAuth = true;
  },

  [SHOW_LOGIN_MODAL](state, value) {
    state.isShowLoginModal = value;
  },

  [CLEAR_USER](state) {
    state.user = null;
    state.isAuth = false;
    state.rentCars = [];
  },

  [SEARCH_CARS](state, data) {
    state.searchCars = data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
